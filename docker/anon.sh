#!/usr/bin/env bash

PGDATA=/var/lib/postgresql/data/

SQL="CREATE EXTENSION IF NOT EXISTS anon CASCADE;"

{
mkdir -p $PGDATA
chown postgres $PGDATA
gosu postgres initdb
gosu postgres pg_ctl start
gosu postgres psql -c "ALTER SYSTEM SET session_preload_libraries = 'anon';"
gosu postgres psql -c "SELECT pg_reload_conf();"
gosu postgres psql --dbname="template1" -c "$SQL"
gosu postgres psql --dbname="postgres" -c "$SQL"

cat | gosu postgres psql
} &> /dev/null

echo '--'
echo '-- Generated by PostgreSQL Anonymizer'
echo "-- date: `date`"
echo '--'

gosu postgres bin/pg_dump_anon.sh --dbname=users
